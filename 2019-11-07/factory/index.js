const Metodo = function(object) {
    objeto = object;
    return {
        map(func){
            if(!object){
                return undefined;
            }
            return ("Just(" + func(object) + ")");
        },
        getType(){
            return "Factory";
        }
    }
  }
  
  const createJust = function(object){
    return Metodo(object);
  };


  const createNothing = ()=>{
    return Metodo();
  };

module.exports ={
    createJust,createNothing,Metodo
};
