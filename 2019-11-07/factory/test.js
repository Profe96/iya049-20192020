const { createJust, createNothing } = require("@profe96/maybe-factory");

const prop = (key, object) =>
  key in object ? createJust(object[key]) : createNothing();

const httpPostMessage = { method: "POST", body: "foobar" };
const httpOptionMessage = { method: "POST" };

console.log(`${prop("foo", { foo: "bar" }).map(word => `${word}!`)}`); // -> Just("foo!")
console.log(`${prop("baz", { foo: "bar" }).map(word => `${word}!`)}`); // -> Nothing
console.log(prop("foo", { foo: "bar" }).getType()); // -> Maybe
console.log(prop("baz", { foo: "bar" }).getType()); // -> Maybe