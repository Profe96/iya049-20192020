//Objeto Maybe
class Maybe{
    constructor(object=null){
        this.object=object;
    }

    map(func){
        if(!this.object){
            return undefined;
        }
        return ("Just(" + func(this.object) + ")");
    }
    getType(){
        return "Maybe";
    }

}

//Funciones createJust y CreateNothing que lo que crean son objetos maybe
const createJust = (object)=>{
    //crear y retornar objeto maybe
    return new Maybe(object);
};
const createNothing = ()=>{
    //crear y retornar objeto maybe
    return new Maybe();
};

//modulo solo deberia exponer las 2 funciones y el objeto
module.exports ={
    createJust,createNothing,Maybe
};
