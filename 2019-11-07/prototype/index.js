function MayBe(object) {
    this.object = object;
    this.type = 'MayBe';
}

MayBe.prototype.map = function(func){

    if(this.object == undefined)
        return 'nothing';
    else
    return ("Just(" + func(this.object) + ")");
    
}

MayBe.prototype.getType = function(func){
    return this.type;
}

const createJust = (object) => {
    return new MayBe(object);
}

const createNothing = () => {
    return new MayBe();
}

module.exports = {createJust, createNothing};